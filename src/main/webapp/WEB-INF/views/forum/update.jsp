<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">개발자 포럼 수정</h3>
        </div>
        <!-- /.box-header -->
        <form id="forumModiForm" method="post" role="form">
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> 질문 주제</strong>
          <p class="text-muted margin-bottom"><input type="text" class="form-control" placeholder="" name="title" id="title" value ="${forum.title }" required> </p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 카테고리</strong>
          <p class="text-muted margin-bottom">
          <select name="categoryId" class="form-control input-sm">
		    <c:forEach items="${categoryList }" var="cate">
		  	  <option value="${cate['CATEGORY_ID']}" ${forum.categoryId eq cate['CATEGORY_ID']?'selected':'' }>${cate['CATE_NAME']}</option>
            </c:forEach>
          </select>
          </p>
          <strong><i class="fa fa-question margin-r-5"></i> 질문 내용</strong>
          <p class="text-muted margin-bottom"> <textarea class="form-control" placeholder="" name="question" id="question" rows="3" required>${forum.question }</textarea></p>
          <hr>
        </div>
        <p class="pull-right">
          <button type="submit" class="btn bg-primary margin" id="btn-mod">수정</button>
	      <a href="/forumList" class="btn bg-primary margin"><b>목록</b></a>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/forumList';
		}

		$('#btn-mod').click(function(event) {
			CommonUtil.postAjaxWithForm('forumModiForm', '/forumModi/${forum.develperForumId}'
					, function() {
						event.preventDefault();
						if($('#title').val().length > 100) {
							alert('질문주제를 100글자 내외로 입력해주세요.');
							$('#title').focus();
							return false;
						}
						if(!confirm('수정하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('수정되었습니다.');
						window.location.href = '/forumList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>