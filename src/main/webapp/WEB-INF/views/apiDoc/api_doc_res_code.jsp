<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-md-6">
		<h3><strong>RESPONSE CODE (응답 코드)</strong></h3>
		<p>
		    클라이언트의 요청에 대해 아래와 같은 응답을 반환한다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    <strong>이름</strong>
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    <strong>코드</strong>
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    SUCCESS
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    S000
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    성공
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    SUCCESS_INTERMEDIATE
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    S001
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    중간 결과 성공
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_UNAUTHORIZATION
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E000
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    인증되지 않은 접근 (유효하지 않은 엑세스 토큰)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NETWORK_ERROR
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E001
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    지니톡 서버 접근 불가 (매니지 서버 접속 불가)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_TIME_OVER
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E002
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    입력 시간 초과 (의미 없는 음성 값들이 지속적으로 들어온 경우)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NO_TOKEN
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E003
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    초기 프로토콜 데이터는 들어 왔으나, 이후 데이터가 들어오지 않는 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NO_SPEECH
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E004
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    음성 입력이 없음 (묵음이 지속 들어오거나 처음부터 묵음이 아닌 음성 소리 잡음 등으로 구성되어 신호는
		                    들어왔으나 음성인식이 안됨)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_INVALID_INPUT
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E005
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    비정상적인 입력 (음성데이터가 아닌 비정상 데이터)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_ENGINE_DOWN
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E006
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    엔진 다운 (엔진이 죽은 경우 - 음성인식 및 기계번역)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_ENGINE_BUSY
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E007
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    엔진 바쁨 (요청 데이터가 채널을 초과한 경우)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NONVALIDATE
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E008
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    일정 시간 동안 사람의 음성이라 판단되지 않는 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_LIMITED_DAY
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E009
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    하루 제한 사용량을 다 사용한 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    INVALID_DATA_FORMAT
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E200
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    데이터 포맷이 유효하지 않을 경우
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
	</div>		
</div>