<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-md-6">
		<h1><strong>API 명세서 소개</strong></h1>
		<p>
		    본 문서는 한컴인터프리에서 제공하는 음성인식 및 번역 API에 대한 호출 방식 및 응답에 대하여 설명을 포함하고 있다.
		</p>
		<h1>
		    <a name="_Toc515531259"><strong>2 API 사용법</strong></a>
		    <strong></strong>
		</h1>
		<p>
		    API의 도메인, API 종류 및 요청 URL 등에 대해 설명한다.
		</p>
		<p>
		    l 도메인 : api.genietalk.com (임시)
		</p>
		<h2>
		    <a name="_Toc515531260"><strong>2.1 API </strong></a>
		    <strong>종류 및 요청 URL</strong>
		    <strong></strong>
		</h2>
		<h3>
		    <a id="voiceRecognition" href="#voiceRecognition"><strong>2.1.1 </strong></a>
		    <strong>음성 인식</strong>
		    <strong></strong>
		</h3>
		<p>
		    음성을 입력 받은 후 인식 결과를 문자열로 반환한다. 인식이 되는 도중에 중간 결과가 수신되며, 완료되면 최종 결과가 수신된다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633">
		                <p align="left">
		                    URI : ws{s}://{<em>도메인명</em>}:9090/recognition/api/v1/voice
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 요청
		</p>
		<p>
		    1. Request Header: Authorization Bearer {access token}
		</p>
		<p>
		    2. Request Parameter
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="161" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="473" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="161" valign="top">
		                <p>
		                    source
		                </p>
		            </td>
		            <td width="473" valign="top">
		                <p>
		                    인식 언어 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 응답
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="163" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="470" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="163" valign="top">
		                <p>
		                    code
		                </p>
		            </td>
		            <td width="470" valign="top">
		                <p>
		                    응답 코드 (RESPONSE CODE표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="163" valign="top">
		                <p>
		                    message
		                </p>
		            </td>
		            <td width="470" valign="top">
		                <p>
		                    응답 코드에 대한 부가적인 설명
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="163" valign="top">
		                <p>
		                    recognitionText
		                </p>
		            </td>
		            <td width="470" valign="top">
		                <p>
		                    인식된 문자열
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="163" valign="top">
		                <p>
		                    completion
		                </p>
		            </td>
		            <td width="470" valign="top">
		                <p>
		                    최종 결과 여부 ( true= 최종 결과, false=중간 결과)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    예)
		</p>
		<p>
		    1. 중간 결과
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": "SUCCESS_MIDDLE",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "completion": false,
		                </p>
		                <p>
		                    "recognitionText": "동해 물과 백두산이 마르고 닳도록."
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    2. 최종 결과
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S001",
		                </p>
		                <p>
		                    "message": "SUCCESS ",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "completion": true,
		                </p>
		                <p>
		                    "recognitionText": "동해 물과 백두산이 마르고 닳도록 안녕하십니까? 반갑습니다. 밥 먹고
		                    합시다."
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<h3>
		    <a id="voiceTranslation" href="#voiceTranslation"><strong>2.1.2 </strong></a>
		    <strong>음성 통역</strong>
		    <strong></strong>
		</h3>
		<p>
		    음성을 입력 받은 후 지정한 언어로 통역한 문자열을 반환한다. 통역이 되는 도중에 중간 결과가 수신되며, 완료되면 최종 결과가
		    수신된다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633">
		                <p>
		                    URI : ws{s}://{<em>도메인명</em>}:9090/translation/api/v1/voice
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 요청
		</p>
		<p>
		    1. Request Header: Authorization Bearer {access token}
		</p>
		<p>
		    2. Request Parameter
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="132" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="501" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="132" valign="top">
		                <p>
		                    source
		                </p>
		            </td>
		            <td width="501" valign="top">
		                <p>
		                    인식 언어 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="132" valign="top">
		                <p>
		                    target
		                </p>
		            </td>
		            <td width="501" valign="top">
		                <p>
		                    통역할 언어 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 응답
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    code
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    응답 코드 (RESPONSE CODE표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    message
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    응답 코드에 대한 부가적인 설명
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    recognitionText
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    통역된 문자열
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    completion
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    최종 결과 여부 ( true= 최종 결과, false=중간 결과)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    inputText
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    입력된 문자열
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    finalText
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    최종 통역된 문자열
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    confidence
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    정확도 (%단위, 최대100, 최소0)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="135" valign="top">
		                <p>
		                    transcript
		                </p>
		            </td>
		            <td width="498" valign="top">
		                <p>
		                    유사 번역 문자열
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    예)
		</p>
		<p>
		    1. 중간 결과
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": "SUCCESS_MIDDLE",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "completion": false,
		                </p>
		                <p>
		                    "recognitionText": "동해 물과 백두산이 마르고 닳도록."
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    2. 유사 번역 문자열이 있는 경우
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "alternatives": [{
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Hello."
		                </p>
		                <p>
		                    }, {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Good afternoon."
		                </p>
		                <p>
		                    }, {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Good evening."
		                </p>
		                <p>
		                    },
		                </p>
		                <p>
		                    ... <em>중간생략</em>
		                </p>
		                <p>
		                    ...
		                </p>
		                <p>
		                    {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "How do you do?"
		                </p>
		                <p>
		                    }],
		                </p>
		                <p>
		                    "inputText": "안녕",
		                </p>
		                <p>
		                    "completion": true,
		                </p>
		                <p>
		                    "finalText": "Hi"
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    3. 유사 번역 문자열이 없는 경우
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "inputText": "hello I am a boy",
		                </p>
		                <p>
		                    "completion": true,
		                </p>
		                <p>
		                    "finalText": "안녕하세요 저는 소년 입니다"
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<h3>
		    <a id="textTranslation" href="#textTranslation"><strong>2.1.3 </strong></a>
		    <strong>문자 번역</strong>
		    <strong></strong>
		</h3>
		<p>
		    입력 받은 문자열을 지정한 언어로 번역한 문자열을 반환한다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633">
		                <p>
		                    URI : ws{s}://{<em>도메인명</em>}:9090/translation/api/v1/text
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 요청
		</p>
		<p>
		    1. Request Header: Authorization Bearer {access token}
		</p>
		<p>
		    2. Request Parameter
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    source
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    입력 문자열의 언어 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    target
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    번역할 언어 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 응답
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    code
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    응답 코드 (RESPONSE CODE표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    message
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    응답 코드에 대한 부가적인 설명
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    finalText
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    최종 번역된 문구
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    confidence
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    정확도 (%단위, 최대100, 최소0)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="109" valign="top">
		                <p>
		                    transcript
		                </p>
		            </td>
		            <td width="525" valign="top">
		                <p>
		                    유사 번역 문자열
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    예)
		</p>
		<p>
		    1. 일반
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "finalText": "안녕하세요 저는 소년 입니다"
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    2. 유사 번역 문자열이 있는 경우
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "alternatives": [{
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Hello."
		                </p>
		                <p>
		                    }, {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Good afternoon."
		                </p>
		                <p>
		                    }, {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "Good evening."
		                </p>
		                <p>
		                    },
		                </p>
		                <p>
		                    ... <em>중간생략</em>
		                </p>
		                <p>
		                    ...
		                </p>
		                <p>
		                    {
		                </p>
		                <p>
		                    "confidence": 100,
		                </p>
		                <p>
		                    "transcript": "How do you do?"
		                </p>
		                <p>
		                    }],
		                </p>
		                <p>
		                    "inputText": "안녕",
		                </p>
		                <p>
		                    "completion": true,
		                </p>
		                <p>
		                    "finalText": "Hi"
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    3. 유사 번역 문자열이 없는 경우
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "inputText": "hello I am a boy",
		                </p>
		                <p>
		                    "completion": true,
		                </p>
		                <p>
		                    "finalText": "안녕하세요 저는 소년 입니다"
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<h3>
		    <a id="supportLanguages" href="#supportLanguages"><strong>2.1.4 </strong></a>
		    <strong>지원 언어 목록</strong>
		    <strong></strong>
		</h3>
		<p>
		    통/번역이 가능한 언어 목록을 반환한다. (port는 80이고 아래처럼 생략 가능)
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633">
		                <p>
		                    URI : http{s}://{<em>도메인명</em>}/translation/api/v1/language
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    ■ 요청
		</p>
		<p>
		    1. Request Header: Authorization Bearer {access token}
		</p>
		<p>
		    ■ 응답
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    code
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    응답 코드 (RESPONSE CODE표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    message
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    응답 코드에 대한 부가적인 설명
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    languages
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    번역이 지원되는 언어 목록 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    예)
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		                <p>
		                    {
		                </p>
		                <p>
		                    "code": "S000",
		                </p>
		                <p>
		                    "message": " SUCCESS",
		                </p>
		                <p>
		                    "result": {
		                </p>
		                <p>
		                    "count": 5,
		                </p>
		                <p>
		                    "languages": [
		                </p>
		                <p>
		                    "KOR",
		                </p>
		                <p>
		                    "ENG",
		                </p>
		                <p>
		                    "CHN",
		                </p>
		                <p>
		                    "JPN",
		                </p>
		                <p>
		                    "ESP"
		                </p>
		                <p>
		                    ]
		                </p>
		                <p>
		                    }
		                </p>
		                <p>
		                    }
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<h1>
		    <a name="_Toc515531265">3 LANGUAGE LIST (지원하는 언어)</a>
		</h1>
		<p>
		    통/번역을 지원하는 언어 목록은 아래와 같다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    <strong>값(alpha-3)</strong>
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		            <td width="369" valign="top">
		                <p>
		                    <strong>비고</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    KOR
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    한국어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ENG
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    영어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    CHN
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    중국어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    JPN
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    일본어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ARA
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    아랍어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ESP
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    스페인어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    FRA
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    프랑스어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    DEU
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    독일어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    RUS
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    러시아어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		    </tbody>
		</table>
		<h1>
		    <a id="responseCode" href="#responseCode">4 RESPONSE CODE (응답 코드)</a>
		</h1>
		<p>
		    클라이언트의 요청에 대해 아래와 같은 응답을 반환한다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    <strong>이름</strong>
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    <strong>코드</strong>
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    SUCCESS
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    S000
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    성공
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    SUCCESS_INTERMEDIATE
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    S001
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    중간 결과 성공
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_UNAUTHORIZATION
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E000
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    인증되지 않은 접근 (유효하지 않은 엑세스 토큰)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NETWORK_ERROR
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E001
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    지니톡 서버 접근 불가 (매니지 서버 접속 불가)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_TIME_OVER
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E002
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    입력 시간 초과 (의미 없는 음성 값들이 지속적으로 들어온 경우)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NO_TOKEN
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E003
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    초기 프로토콜 데이터는 들어 왔으나, 이후 데이터가 들어오지 않는 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NO_SPEECH
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E004
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    음성 입력이 없음 (묵음이 지속 들어오거나 처음부터 묵음이 아닌 음성 소리 잡음 등으로 구성되어 신호는
		                    들어왔으나 음성인식이 안됨)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_INVALID_INPUT
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E005
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    비정상적인 입력 (음성데이터가 아닌 비정상 데이터)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_ENGINE_DOWN
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E006
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    엔진 다운 (엔진이 죽은 경우 - 음성인식 및 기계번역)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_ENGINE_BUSY
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E007
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    엔진 바쁨 (요청 데이터가 채널을 초과한 경우)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_NONVALIDATE
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E008
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    일정 시간 동안 사람의 음성이라 판단되지 않는 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    ERROR_LIMITED_DAY
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E009
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    하루 제한 사용량을 다 사용한 경우
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="205" valign="top">
		                <p>
		                    INVALID_DATA_FORMAT
		                </p>
		            </td>
		            <td width="86" valign="top">
		                <p>
		                    E200
		                </p>
		            </td>
		            <td width="350" valign="top">
		                <p>
		                    데이터 포맷이 유효하지 않을 경우
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>

   	</div>
</div>

<script type="text/javascript">
	var page = '${page}';
	console.log(page);
	if (page) {
		console.log('gogogo');
		window.location.href = '/apiDocument#' + page;
	}
	
	$(function() {
/* 	 	if (window.location.hash) {
			var hash = window.location.hash;
			console.log('/apiDocument' + hash);
			window.location.href = '/apiDocument' + hash;
			
			//var offset = $(hash).offset();
			//$('html, body').animate({scrollTop : offset.top}, 100);
		} */
	});
</script>