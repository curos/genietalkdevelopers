<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-md-6">
		<h3><strong>LANGUAGE LIST (지원하는 언어)</strong></h1>
		<p>
		    통/번역을 지원하는 언어 목록은 아래와 같다.
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    <strong>값(alpha-3)</strong>
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		            <td width="369" valign="top">
		                <p>
		                    <strong>비고</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    KOR
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    한국어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ENG
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    영어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    CHN
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    중국어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    JPN
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    일본어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ARA
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    아랍어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ESP
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    스페인어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    FRA
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    프랑스어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    DEU
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    독일어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    RUS
		                </p>
		            </td>
		            <td width="113" valign="top">
		                <p>
		                    러시아어
		                </p>
		            </td>
		            <td width="369" valign="top">
		            </td>
		        </tr>
		    </tbody>
		</table>
	</div>		
</div>