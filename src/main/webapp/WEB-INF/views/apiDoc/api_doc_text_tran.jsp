<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-md-6">
		<h3><strong>문자 번역 (WebSocket)</strong></h3>
<p>
    입력 받은 문자열을 지정한 언어로 번역한 문자열을 반환한다.
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633">
                <p>
                    URI : ws{s}://{<em>도메인명</em>}:8080/translation/api/v1/text
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    ■ 요청
</p>
<p>
    1. Request Header: Authorization Bearer {access token}
</p>
<p>
    2. Request Parameter
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="104" valign="top">
                <p>
                    <strong>키</strong>
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    <strong>설명</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    slang
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    인식 언어 (LANGUAGE LIST표 참조)
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    tlang
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    통역할 언어 (LANGUAGE LIST표 참조)
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    3. Request data
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="104" valign="top">
                <p>
                    <strong>키</strong>
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    <strong>설명</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    requestText
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    번역하고자 하는 text.
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    ■ 응답
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="109" valign="top">
                <p>
                    <strong>키</strong>
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    <strong>설명</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    code
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    응답 코드 (RESPONSE CODE표 참조)
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    message
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    응답 코드에 대한 부가적인 설명
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    finalText
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    최종 번역된 문구
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    confidence
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    정확도 (%단위, 최대100, 최소0)
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    transcript
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    유사 번역 문자열
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    예)
</p>
<p>
    1. 일반
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
				<pre style="margin-bottom: 0px;">
{
	"code": "S000",
	"message": " SUCCESS",
	"result": {
		"finalText": "안녕하세요 저는 소년 입니다"
	}
}
				</pre>
            </td>
        </tr>
    </tbody>
</table>
<p>
    2. 유사 번역 문자열이 있는 경우
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
				<pre style="margin-bottom: 0px;">
{
	"code": "S000",
	"message": " SUCCESS",
	"result": {
		"alternatives": [{
				"confidence": 100,
				"transcript": "Hello."
			}, {
				"confidence": 100,
				"transcript": "Good afternoon."
			}, {
				"confidence": 100,
				"transcript": "Good evening."
			},
			...중간생략
			...{
				"confidence": 100,
				"transcript": "How do you do?"
			}
		],
		"inputText": "안녕",
		"completion": true,
		"finalText": "Hi"
	}
}
				</pre>
            </td>
        </tr>
    </tbody>
</table>
<p>
    3. 유사 번역 문자열이 없는 경우
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
				<pre style="margin-bottom: 0px;">
{
	"code": "S000",
	"message": " SUCCESS",
	"result": {
		"inputText": "hello I am a boy",
		"completion": true,
		"finalText": "안녕하세요 저는 소년 입니다"
	}
}
				</pre>
            </td>
        </tr>
    </tbody>
</table>
<h3>
    <a name="_Toc519157004"><strong>2.1.6 </strong></a>
    <strong>문자 번역 (HTTP)</strong>
    <strong></strong>
</h3>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
                <p>
                    URI : http{s}://{<em>도메인명</em>}/translation/api/v1/text
                </p>
                <p>
                    Method : POST
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    ■ 요청
</p>
<p>
    1. Request Header
</p>
<p>
    l Authorization: Bearer {access token}
</p>
<p>
    l Content-Type: application/json
</p>
<p>
    2. Request Body
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="104" valign="top">
                <p>
                    <strong>키</strong>
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    <strong>설명</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    requestText
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    번역하고자 하는 text
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    srcLang
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    번역 전 input 언어(LANGUAGE LIST표 참조)
                </p>
            </td>
        </tr>
        <tr>
            <td width="104" valign="top">
                <p>
                    tgtLang
                </p>
            </td>
            <td width="529" valign="top">
                <p>
                    번역 후 output 언어 (LANGUAGE LIST표 참조)
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    예)
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
				<pre style="margin-bottom: 0px;">
{
	"code": "S002",
	"message": "TEXT_TRANSLATION_SUCCESS",
	"result": {
		"inputText": "나는 당신을 사랑합니다",
		"finalText": "I love you, too."
	}
}
				</pre>
            </td>
        </tr>
    </tbody>
</table>
<p>
    ■ 응답
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="109" valign="top">
                <p>
                    <strong>키</strong>
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    <strong>설명</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    code
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    응답 코드 (RESPONSE CODE표 참조)
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    message
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    응답 코드에 대한 부가적인 설명
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    finalText
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    최종 번역된 문구
                </p>
            </td>
        </tr>
        <tr>
            <td width="109" valign="top">
                <p>
                    inputText
                </p>
            </td>
            <td width="525" valign="top">
                <p>
                    입력된 문자열
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    예)
</p>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="633" valign="top">
				<pre style="margin-bottom: 0px;">
{
	"code": "S002",
	"message": "TEXT_TRANSLATION_SUCCESS",
	"result": {
		"inputText": "나는 당신을 사랑합니다",
		"finalText": "I love you, too."
	}
}
				</pre>
            </td>
        </tr>
    </tbody>
</table>
	</div>		
</div>