<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-md-6">
		<h3><strong>지원 언어 목록</strong></h3>
		<p>
		    통/번역이 가능한 언어 목록을 반환한다. (port는 80이고 아래처럼 생략 가능)
		</p>
		<p>
		   이 API는 HTTP방식만 지원한다.
		</p>		
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633">
		                <p>
		                    URI : http{s}://{<em>도메인명</em>}/translation/api/v1/language<br/>
		                    Method : GET
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<br/>
		<p>
		    ■ 요청
		</p>
		<p>
		    1. Request Header: Authorization Bearer {access token}
		</p>
		<p>
		    ■ 응답
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    <strong>키</strong>
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    code
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    응답 코드 (RESPONSE CODE표 참조)
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    message
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    응답 코드에 대한 부가적인 설명
		                </p>
		            </td>
		        </tr>
		        <tr>
		            <td width="104" valign="top">
		                <p>
		                    languages
		                </p>
		            </td>
		            <td width="529" valign="top">
		                <p>
		                    번역이 지원되는 언어 목록 (LANGUAGE LIST표 참조)
		                </p>
		            </td>
		        </tr>
		    </tbody>
		</table>
		<p>
		    예)
		</p>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="633" valign="top">
		            	<pre style="margin-bottom: 0px;">
{
	"code": "S000",
	"message": "SUCCESS",
	"result": {
		"count": 9,
		"languages": [
			"KOR",
			"ENG",
			"CHN",
			"JPN",
			"ARA",
			"ESP",
			"FRA",
			"DEU",
			"RUS"
		]
	}
}
		            	</pre>
		            </td>
		        </tr>
		    </tbody>
		</table>
	</div>		
</div>