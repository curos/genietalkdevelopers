<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-lg-6 col-lg-offset-3" id="alert-div" style="display: none;">
		<div class="alert alert-danger alert-dismissible" id=alert-color-div role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">×</span>
				<span class="sr-only">Close</span>
			</button>
			<p id="alert-msg"></p>
		</div>
	</div>

	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">애플리케이션의 기본적인 정보들을 등록합니다</h3>
        </div>
        <!-- /.box-header -->
        <form id="addAppForm" method="post" role="form">
        <div class="box-body">
          <strong><i class="fa fa-map-marker margin-r-5"></i> 애플리케이션 이름</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="appName" id="appName" required></p>
          <span class="block">내 애플리케이션 리스트로 표시되는 이름으로 가급적 10자 내외로 본인이 잘 인식할수 있는 이름을 사용해 주세요.</span>
          <hr>
          <strong><i class="fa fa-pencil margin-r-5"></i> 애플리케이션 설명</strong>
          <p class="text-muted"> <textarea class="form-control" placeholder="이 애플리케이션 설명을 자세히 적으세요" name="appDescription" id="appDescription" required></textarea></p>
          <hr>
        </div>
        <p class="pull-right">
	      <button type="submit" class="btn bg-primary margin" id="btn-add"><b>등록</b></button>
	      <a href="/myAppList" class="btn bg-primary margin"><b>취소</b></a>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#btn-add').click(function(event) {
			CommonUtil.postAjaxWithForm('addAppForm', '/myAppReg'
					, function() {
						event.preventDefault();
						if($('#appName').val().length > 10) {
							alert('애플리케이션 이름은 10글자 내외로 입력해주세요.');
							$('#appName').focus();
							return false;
						}
						if(!confirm('등록하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('이메일로 Client ID와 Secret을 보내드렸습니다.\n이것은 본인의 패스워드와 같으니 보안에 유의 하시기 바랍니다.');
						window.location.href = '/myAppList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>