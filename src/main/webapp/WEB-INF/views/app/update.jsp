<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
    <div class="box box-primary">
       <div class="box-header with-border">
         <h3 class="box-title">애플리케이션의 기본적인 정보들을 수정합니다</h3>
       </div>
       <!-- /.box-header -->
       <!-- form start -->
       <form id="modiAppForm" method="post" role="form">
        <div class="box-body">
          <input type="hidden" name="clientId" value="${myClient.clientId }">
          <strong><i class="fa fa-angle-double-right margin-r-5"></i> Client ID</strong>
          <p class="text-muted"> ${myClient.clientId }</p>
          <hr>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 애플리케이션 이름</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="appName" id="appName" value="${myClient.appName}" required></p>
          <span class="block">내 애플리케이션 리스트로 표시되는 이름으로 가급적 10자 내외로 본인이 잘 인식할수 있는 이름을 사용해 주세요.</span>
          <hr>
          <strong><i class="fa fa-pencil margin-r-5"></i> 애플리케이션 설명</strong>
          <p class="text-muted"> <textarea class="form-control" placeholder="이 애플리케이션 설명을 자세히 적으세요" name="appDescription" id="appDescription" required>${myClient.appDescription }</textarea></p>
          <hr>
        </div>
        <p class="pull-right">
	      <button type="submit" class="btn bg-primary margin" id="btn-mod"><b>수정</b></button>
	      <a href="/myAppDetail/${myClient.clientId}" class="btn bg-primary margin"><b>취소</b></a>
        </p>
        </form>
     </div>
     <!-- /.box -->
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#btn-mod').click(function(event) {
			CommonUtil.postAjaxWithForm('modiAppForm', '/myAppModi/${myClient.clientId}'
					, function() {
						event.preventDefault();
						if($('#appName').val().length > 10) {
							alert('애플리케이션 이름은 10글자 내외로 입력해주세요.');
							$('#appName').focus();
							return false;
						}
						if(!confirm('수정하시겠습니까?'))
							return false;
						else
							return true;
					}
					, function(data) {
						alert('수정하였습니다.');
						window.location.href = '/myAppDetail/${myClient.clientId}';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>