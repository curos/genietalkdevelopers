<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row margin-bottom">
	<div class="col-md-6">
		<h3><strong>지니톡 자동 통역  API 목록</strong></h3>
		<table border="1" cellspacing="0" cellpadding="0">
		    <tbody>
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    <strong>API 명</strong>
		                </p>
		            </td>
		            <td width="369" valign="top">
		                <p>
		                    <strong>설명</strong>
		                </p>
		            </td>
		        </tr>
		        <c:forEach items="${apiList}" var="api">
		        <tr>
		            <td width="158" valign="top">
		                <p>
		                    ${api.apiName }
		                </p>
		            </td>
		            <td width="369" valign="top">
		            	<p>
		                    ${api.descript }
		                </p>
		            </td>
		        </tr>
		        </c:forEach>
		    </tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
<a href="/myAppReg" class="btn bg-primary"><b>애플리케이션 등록</b></a>
</div>
</div>