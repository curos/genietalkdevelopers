<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">애플리케이션의 정보들을 조회/수정합니다</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> Client ID</strong>
          <p class="text-muted">${myClient.clientId }</p>
          <hr>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 애플리케이션 이름</strong>
          <p class="text-muted"> ${myClient.appName }</p>
          <hr>
          <strong><i class="fa fa-pencil margin-r-5"></i> 애플리케이션 설명</strong>
          <p class="text-muted"> ${myClient.appDescription }</p>
          <hr>
        </div>
        <form id="clientForm" method="post" role="form" action="/myAppDel">
          <input type="hidden" name="clientId" value="${myClient.clientId}">
        </form>
        <p class="pull-right">
	      <button type="button" class="btn bg-primary margin" id="btn-analy"><b>사용현황</b></button>
	      <button type="button" class="btn bg-primary margin" id="btn-mod">수정</button>
	      <button type="button" class="btn bg-primary margin" id="btn-del">삭제</button>
	      <a href="/myAppList" class="btn bg-primary margin"><b>목록</b></a>
        </p>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#btn-del').click(function(event) {
			if(!confirm('삭제하시겠습니까?'))
				return;
			CommonUtil.postAjaxWithForm('clientForm', '/myAppDel'
					, function() {
						event.preventDefault();
						return true;
					}
					, function(data) {
						alert('삭제되었습니다.');
						window.location.href = '/myAppList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
		
		$('#btn-mod').click(function(event) {
			window.location.href = '/myAppModi/${myClient.clientId}';
		});

		$('#btn-analy').click(function(event) {
			window.location.href = '/myAppAnaly/${myClient.clientId}';
		});
	});
</script>