<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="row">
	    <p class="pull-right">
	      <a href="/myAppReg" class="btn bg-primary margin"><b>애플리케이션 등록</b></a>
	      <button type="button" class="btn bg-primary margin" id="btn-del">삭제</button>
        </p>
	  </div>
	  <div class="box">
	    <div class="box-body table-responsive no-padding">
	      <form id="clientForm" method="post" role="form" style="display: block;" >
	      <table class="table table-hover">
	        <tbody><tr>
	          <th>전체</th>
	          <th>Client ID</th>
	          <th>Application 이름</th>
	        </tr>
	        <c:forEach items="${myClientList}" var="app">
	        <tr>
	          <td><input type="checkbox" name="clientId" id="clientId" value="${app.clientId }"/></td>
	          <td><a href="/myAppDetail/${app.clientId}">${app.clientId }</a></td>
	          <td>${app.appName }</td>
	        </tr>
	        </c:forEach>
	      </tbody>
	      </table>
	      </form>
	    </div>
	    <!-- /.box-body -->
	  </div>
	  <!-- /.box -->
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#btn-del').click(function(event) {
			if($('#clientId:checked').length < 1) {
				alert('삭제하실 애플리케이션을 선택해주세요');
				return;
			}
			if(!confirm('삭제하시겠습니까?'))
				return;
			CommonUtil.postAjaxWithForm('clientForm', '/myAppDel'
					, function() {
						event.preventDefault();
						return true;
					}
					, function(data) {
						alert('삭제되었습니다.');
						window.location.href = '/myAppList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>