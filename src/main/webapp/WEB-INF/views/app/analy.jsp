<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-7">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">애플리케이션의 사용 현황을 조회합니다.</h3>
        </div>
        <!-- /.box-header -->
        <div class="row">
          <div class="box-body">
	        <div class="col-md-12">
	        <form id="searchForm" method="post" role="form">
	          <div class="form-group">
		        <div class="input-group">
		          <div class="input-group-addon">
		            <i class="fa fa-calendar"></i>
		          </div>
		          <input type="text" class="form-control pull-right" id="searchDate" name="searchDate" readonly>
		          <span class="input-group-btn">
	                 <button class="btn bg-primary" id="btn-search">사용현황</button>
	              </span>
		        </div>
	          </div>
	        </form>
	        </div>
	      </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
              <strong><i class="fa fa-book margin-r-5"></i> Client ID</strong>
              <p class="text-muted">${clientId }</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
	          <table class="table table-hover" id="analysisTable">
		        <tbody>
		        <tr>
		          <th>API 명</th>
		          <th>호출 수</th>
		          <th>호출 성공 수</th>
		        </tr>
		        </tbody>
	          </table>
	        </div>
          </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		getAnalysisData(false);
		
		//Date range picker
		var start = '${start}';
		var end = '${end}';
	    $('#searchDate').daterangepicker({
			opens: 'left',
			locale: {
				format: 'YYYY/MM/DD'
			},
	   	    startDate: start,
	   	    endDate: end,
	   	    ranges: {
		   	    'Today': [moment(), moment()],
		   	    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		   	    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		   	    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		   	    'This Month': [moment().startOf('month'), moment().endOf('month')],
		   	    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	   	    }
	    });
	    
	    $('#btn-search').click(function(event) {
	    	getAnalysisData(true);
		});
	});
	
	function getAnalysisData(isClick) {
		CommonUtil.postAjaxWithForm(
				'searchForm'
				, '/myAppAnaly/${clientId}'
				, function() {
					if (isClick) {
						event.preventDefault();						
					}
					return true;
				}
				, function(data) {
					var source   = document.getElementById("analysisTable-template").innerHTML;
					var template = Handlebars.compile(source);
					var html = template(data.contents);
					$('#analysisTable').html(html);
				}
				, function(data) {
					alert('데이터 수신 실패');
				}
		);
	}
</script>

<script id="analysisTable-template" type="text/x-handlebars-template">
<tbody>
	<tr>
		<th>API 명</th>
		<th>호출 수</th>
		<th>호출 성공 수</th>
	</tr>
{{#each this}}
	<tr>
		<td>{{appName}}</td>
		<td>{{cnt}}</td>
		<td>{{successCnt}}</td>
	</tr>
{{/each}}
</tbody>
</script>