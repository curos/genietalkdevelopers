<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-md-6">
		<div class="" id="alert-div" style="display: none;">
			<div class="alert alert-danger alert-dismissible" id=alert-color-div role="alert">
				<button type="button" class="close" onclick="hideErrorAlert(event)">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<p id="alert-msg"></p>
			</div>
		</div>
		
		<div class="" id="change-div">
			<h4 class="">비밀번호 재확인</h4>
			<form accept-charset="UTF-8" role="form" id="confirmPwForm" method="post" action="/confirmPw">
				<fieldset>
					<span class="help-block"> 회원님의 정보를 안전하게 보호하기 위해,
					<br> 개인정보를 수정하기 전에 비밀번호를 다시 한번 확인합니다.
					<br> 비밀번호가 일치하면 6자리 보안코드가 가입자의 이메일로 발송 합니다.
					</span>
					<div class="form-group input-group">
						<span class="input-group-addon"> 비밀번호 </span> 
						<input class="form-control" placeholder="Password" name="password" id="password" type="password" required >
					</div>
					<button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">확인</button>
				</fieldset>
			</form>
		</div>
		
		<a href="#" data-toggle="modal" data-target="#login-modal" id="changePwModalLink" style="display: none;">Login</a>
		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>비밀번호 변경</h1>
					<div class="card-body">
						<form class="form" id="changePwModalForm" role="form" autocomplete="off" action="/changePw" method="post">
							<div class="form-group">
								<label for="passwordOld">기존(임시) 비밀번호</label>
								<input type="password" class="form-control" id="passwordOld" name="passwordOld" required>
							</div>
							<div class="form-group">
								<label for="passwordNew">신규 비밀번호</label>
								<input type="password" class="form-control" id="passwordNew" name="passwordNew" pattern="(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,16}" required>
								<span class="form-text small text-muted">
									영문,숫자,특수문자 조합, 6~16자리
								</span>
							</div>
							<div class="form-group">
								<label for="inputPasswordNewVerify">신규 비밀번호 확인</label>
								<input type="password" class="form-control" id="passwordNewConfirm" required>
							</div>
							<div class="form-group">
								<label for="securityCode">보안코드(6자리)</label>
								<input type="password" class="form-control" id="securityCode" name="securityCode" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-lg float-right" id="changePwSubmit">변경</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function() {
		
		$('#btn-olvidado').click(function(event) {
			CommonUtil.postAjaxWithForm(
					'confirmPwForm'
					, '/confirmPw'
					, function() {
						$('#btn-olvidado').prop("disabled",true);
						event.preventDefault();
						return true;
					}
					, function(data) {
						alert('보안코드가 발송되었습니다.');
						$('#btn-olvidado').prop("disabled",false);
						$('#changePwModalLink').trigger('click');
						hideErrorAlert();
					}
					, function(data) {
						$('#btn-olvidado').prop("disabled",false);
						showErrorAlert(data.resultMsg);
					}
			);
		});
		
		$('#changePwSubmit').click(function(event) {
			CommonUtil.postAjaxWithForm(
					'changePwModalForm'
					, '/changePw'
					, function() {
						event.preventDefault();
						
						if ($('#passwordOld').val() == $('#passwordNew').val()) {
							alert('기존 비밀번호와 신규 비밀번호가 일치 합니다.\n신규 비밀번호를 다시 입력해 주세요.');
							$('#passwordNew').focus();
							return false;
						}
						
						if ($('#passwordNew').val() != $('#passwordNewConfirm').val()) {
							alert('신규 비밀번호 확인 값이 일치 하지 않습니다.');
							$('#passwordNewConfirm').focus();
							return false;
						}
						return true;
					}
					, function(data){
						alert('비밀번호가 변경 되었습니다.');
						window.location.href = '/main';
					}
			);
		});
	});
	
	function showErrorAlert(errorMsg) {
		$('#alert-msg').html(errorMsg);
		$('#alert-div').show();	
	}
	
	function hideErrorAlert(e) {
		if (e) {
			e.preventDefault();
		}
		$('#alert-msg').html('');
		$('#alert-div').hide();
	}
</script>