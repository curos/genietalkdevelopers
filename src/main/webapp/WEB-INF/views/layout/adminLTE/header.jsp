<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<%=request.getContextPath() %>/images/logo.png" alt="logo"/></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <sec:authorize access="isAnonymous()">
          <li class="user user-menu">
			<a href="/login">Login</a>
		  </li>
		  </sec:authorize>
          <sec:authorize access="isAuthenticated()">
          <li class="dropdown user user-menu">          	
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">${accountInfo.email}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header" style="height: auto;">
                <p>
                  ${accountInfo.name} - ${accountInfo.company}
                  <small>${accountInfo.mobile}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/accountInfo" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          </sec:authorize>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>