<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview api_doc_voice_rec api_doc_voice_tran api_doc_text_tran api_doc_support_lang">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>자동통역 API매뉴얼</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="api_doc_voice_rec"><a href="/apiDocument/voiceRecognition"><i class="fa fa-circle-o"></i> 음성인식</a></li>
            <li class="api_doc_voice_tran"><a href="/apiDocument/voiceTranslation"><i class="fa fa-circle-o"></i> 음성통역</a></li>
            <li class="api_doc_text_tran"><a href="/apiDocument/textTranslation"><i class="fa fa-circle-o"></i> 문자번역</a></li>
            <li class="api_doc_support_lang"><a href="/apiDocument/supportLanguages"><i class="fa fa-circle-o"></i> 지원언어 목록</a></li>
          </ul>
        </li>
        <li class="api_doc_response_code">
          <a href="/apiDocument/responseCode">
            <i class="fa fa-th"></i> <span>응답 코드</span>
           </a>
        </li>
        <li class="api_doc_lang_list">
          <a href="/apiDocument/supportLangList">
            <i class="fa fa-th"></i> <span>지원하는 언어</span>
          </a>
        </li>
        <li class="treeview api_list app_list app_add app_detail app_update app_analy">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>자동 통역 API</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="app_list app_detail app_update app_analy"><a href="/myAppList"><i class="fa fa-circle-o"></i> 내 애플리케이션</a></li>
            <li class="app_add"><a href="/myAppReg"><i class="fa fa-circle-o"></i> 애플리케이션 등록</a></li>
          </ul>
        </li>
        <li class="change_password">
          <a href="/changePw">
            <i class="fa fa-calendar"></i> <span>비밀번호 수정</span>
          </a>
        </li>
        <li class="forum_list forum_add forum_update forum_detail">
          <a href="/forumList">
            <i class="fa fa-th"></i> <span>개발자포럼</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <script>
  $(function() {
		//menu
		$(".api_list ").click(function() {
			window.location.href = '/apiList';
		});
		$("."+"${menu}").addClass("active");
	});
  </script>