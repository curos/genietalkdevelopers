package com.curos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curos.dao.ApiMapper;
import com.curos.dto.ApiDto;

@Service
public class ApiService {

	@Autowired
	private ApiMapper apiMapper;
	
	/**
	 * 앱 사용현황 조회
	 * @param email
	 * @param clientId
	 * @return
	 * @throws Exception 
	 */
	public List<ApiDto> selectApiAllList() {
		return apiMapper.selectApiAllList();
	}
	
}
