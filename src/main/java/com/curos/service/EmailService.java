package com.curos.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.curos.util.CustomStringUtils;
import com.curos.util.CustomStringUtils.RandomMode;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * 이메일 서비스
 */
@Service
public class EmailService {

	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private JavaMailSender sender;
	
    @Autowired
    private Configuration freemarkerConfig;
    
    @PostConstruct
    public void init() {
        // set loading location to src/main/resources
        // You may want to use a subfolder such as /templates here
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/email/");
    }
    
    /**
     * 임시 비밀번호 전송
     * @param email
     * @param onNext
     * @param onError
     * @param isBlocking
     */
	public Observable<String> sendTempPassword(String email) {
    	return Observable.defer(() -> {
    		logger.info("임시 비번 생성 및 전송");
        	String tempPassword = CustomStringUtils.random(15, RandomMode.ALL);
            Map<String, Object> model = new HashMap<>();
            model.put("tempPassword", tempPassword);
            send("임시비밀번호발급", email, "TempPassword.ftl", model);
    		return Observable.just(tempPassword);
    	})
    	.subscribeOn(Schedulers.io());
    }

    /**
     * 보안코드 발송
     * @param email
     * @return
     * @throws MessagingException
     * @throws IOException
     * @throws TemplateException
     */
	public String sendSecurityCode(String email) throws MessagingException, IOException, TemplateException {
        String securityCode = CustomStringUtils.random(6, RandomMode.NUMBER);
        Map<String, Object> model = new HashMap<>();
        model.put("securityCode", securityCode);
        send("보안코드발급", email, "SecurityCode.ftl", model);
        return securityCode;
	}
	
	public void sendClientSecret(String email, String clientId, String secret) throws MessagingException, IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("clientId", clientId);
        model.put("secret", secret);
        send("애플리케이션 등록", email, "ApplicationInfo.ftl", model);
	}

	/**
	 * 이메일 발송
	 * @param subject
	 * @param email
	 * @param tmpFileName
	 * @param model
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	private void send(String subject, String email, String tmpFileName, Map<String,Object> model) throws MessagingException, IOException, TemplateException {
    	MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        Template template = freemarkerConfig.getTemplate(tmpFileName);
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        
        helper.setFrom("interfree@gmail.com", "인터프리");
        helper.setSubject(subject);
        helper.setTo(email);
        helper.setText(text, true); // set to html
        sender.send(message);
	}
}
