package com.curos.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.curos.dao.MyAppMapper;
import com.curos.dto.AccountDto;
import com.curos.dto.OauthClientDetailsDto;
import com.curos.util.CustomStringUtils;
import com.curos.util.DateTimeUtils;
import com.curos.util.CustomStringUtils.RandomMode;

import freemarker.template.TemplateException;

@Service
public class MyAppService {
	@Value("${oauth.server.host}")
	private String oauthServerHost;
	
	@Value("${oauth.server.port}")
	private Integer oauthServerPort;
	
	@Autowired
	private MyAppMapper myAppMapper;
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	private Base64.Encoder encoder = Base64.getEncoder().withoutPadding();
	
	private final Log logger = LogFactory.getLog(getClass());
	
	// TODO : 나중에 Oauth2 정책에 따라 수정
	private final String GRANT_TYPE = "client_credentials,refresh_token";
	private final String SCOPE = "read,write";
	private final int TOKEN_VALIDITY = 60 * 60 * 24 * 365;

	/**
	 * OAuth 생성 및 등록
	 * @param accountDto
	 * @param oauthClient
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	public int registerOauthClient(AccountDto accountDto, OauthClientDetailsDto oauthClient) throws MessagingException, IOException, TemplateException {
		oauthClient.setClientId(getClientId(accountDto.getCompany()));
		String secret = getClientSecret();
		oauthClient.setClientSecret(secret);
		oauthClient.setAuthorizedGrantTypes(GRANT_TYPE);
		oauthClient.setScope(SCOPE);
		oauthClient.setAccessTokenValidity(TOKEN_VALIDITY);
		oauthClient.setAccountId(accountDto.getAccountId());
		oauthClient.setAppDevState("1");
		
		int result = registerOauthClient(oauthClient);
		
		if(result == 1) {
			emailService.sendClientSecret(accountDto.getEmail(), oauthClient.getClientId(), secret);
		}
		
		return result;
	}
	
	/**
	 * OAuth 정보 등록
	 * @param oauthClient
	 * @return
	 */
	public int registerOauthClient(OauthClientDetailsDto oauthClient) {
		// ClientSecret값은 passwordEncoder로 변환된 값을 Insert 해줘야 한다.
		oauthClient.setClientSecret(passwordEncoder.encode(oauthClient.getClientSecret()));

		return myAppMapper.insertOauthClientDetail(oauthClient);
	}

	/**
	 * Oauth에 저장할 Client ID값 획득 
	 * @param company
	 * @return
	 */
	public String getClientId(String company) {
		// 고객사 이름 + yyyymmddhhmmss 의 스트링 생성
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String clientId = company + sdf.format(new Date());

		// 위 값을 Base64로 인코딩 하여 완료
		return encoder.encodeToString(clientId.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * Oauth에 저장할 Client Secret값 획득 
	 * @return
	 */
	public String getClientSecret() {
		String clientSecret = null;
		try {
			// 랜덤한 문자열을 생성
			String random = CustomStringUtils.random(20, RandomMode.ALL);

			// salt값 랜덤 하게 생성 (java.security.SecureRandomt 사용)
			byte[] salt = generateSalt();

			// random값과 salt값을 이용하여 SHA-256 해싱
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();
			digest.update(salt);
			byte[] saltedRandom = digest.digest(random.getBytes());

			// 위의 값을 Base64로 인코딩 하여 완료
			clientSecret = new String(encoder.encode(saltedRandom));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return clientSecret;
	}

	private byte[] generateSalt() {
		SecureRandom secureRandom = new SecureRandom();
		byte[] salt = new byte[20];
		secureRandom.nextBytes(salt);
		return salt;
	}

	/**
	 * 내 애플리케이션 목록 조회
	 * @param accountId
	 * @return
	 */
	public List<OauthClientDetailsDto> getClientDetailByAccountId(int accountId){
		return myAppMapper.selectOauthClientDetailByAccountId(accountId);
	}
	
	/**
	 * Client 정보 조회
	 * @param paramMap
	 * @return
	 */
	public OauthClientDetailsDto getClientDetailByClientId(Map<String, Object> paramMap){
		return myAppMapper.selectOauthClientDetailByClientId(paramMap);
	}
	
	/**
	 * 클라이언트 애플리케이션 수정
	 * @param oauthClientDetailsDto
	 * @return
	 */
	public int updateClientDetail(OauthClientDetailsDto oauthClientDetailsDto){
		return myAppMapper.updateOauthClientDetail(oauthClientDetailsDto);
	}
	
	/**
	 * 클라이언트 애플리케이션 삭제
	 * @param accountDto
	 * @param clientId
	 * @return
	 */
	public int deleteClientDetail(AccountDto accountDto, String clientId){
		String[] clientArray = clientId.split(",");
		int count = 0;
		for (String client : clientArray) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			int accountId = accountDto.getAccountId();
			paramMap.put("accountId", accountId);
			paramMap.put("clientId", client);

			OauthClientDetailsDto myClientList = myAppMapper.selectOauthClientDetailByClientId(paramMap);
			if (myClientList != null && deleteToken(client) && myAppMapper.deleteOauthClientDetail(paramMap) == 1) {
				logger.info("clientId 삭제 성공: " + client);
				count++;
			} else {
				logger.error("clientId 삭제 실패 : " + client);
			}
		}
		
		return (count == clientArray.length) ? 1 : 0;
	}
	
	/**
	 * OAuth Server로 Token 삭제 요청
	 * @param clientId
	 * @return
	 */
	private boolean deleteToken(String clientId) {
		boolean result = false;
		try {
			String URL = oauthServerHost + ":" + oauthServerPort + "/oauth/revoke?clientId="+clientId;

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(URL);
			HttpResponse response = client.execute(httpGet);

			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				result = true;
			}
		} catch (Exception e) {
			logger.error("토큰 서버 연결 실패 : " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * 앱 사용현황 조회
	 * @param email
	 * @param clientId
	 * @return
	 * @throws Exception 
	 */
	public List<Map<String,Object>> selectAnalysis(String email, String clientId, String searchDate) throws Exception {
		Map<String,Object> paramMap = Optional.ofNullable(searchDate)
			.map(dateStr -> dateStr.split(" - "))
			.filter(array -> array.length == 2)
			.filter(array -> StringUtils.isNotBlank(array[0]))
			.filter(array -> StringUtils.isNotBlank(array[1]))
			.map(array -> {
				Map<String,Object> map = new HashMap<>();
				map.put("startDate", array[0]);
				map.put("endDate", array[1]);
				return map;
			}).orElseGet(() -> {
				String now = DateTimeUtils.getNowDateStr();
				Map<String,Object> map = new HashMap<>();
				map.put("startDate", now);
				map.put("endDate", now);
				return map;
			});
		
		paramMap.put("email", email);
		paramMap.put("clientId", clientId);
		return myAppMapper.selectAnalysis(paramMap);
	}
}