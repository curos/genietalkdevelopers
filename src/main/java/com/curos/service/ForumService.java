package com.curos.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curos.dao.ForumMapper;
import com.curos.dto.ForumDto;

@Service
@Transactional
public class ForumService {
	@Autowired
	private ForumMapper forumMapper;

	public int getForumListCount(Map<String, Object> searchMap) {
		return forumMapper.selectForumListCount(searchMap);
	}

	public List<ForumDto> getForumList(Map<String, Object> searchMap) {
		return forumMapper.selectForumList(searchMap);
	}

	public List<Map<String, Object>> selectForumCategoryList() {
		return forumMapper.selectForumCategoryList();
	}

	public int insertForumQuestion(ForumDto forumDto) {
		return (forumMapper.insertForumQuestion(forumDto) & forumMapper.insertForumCategory(forumDto));
	}
	
	public ForumDto getForumDetail(int forumId) {
		return forumMapper.selectForumDetail(forumId);
	}
	
	public int updateForumQuestion(ForumDto forumDto) {
		return (forumMapper.updateForumQuestion(forumDto) & forumMapper.updateForumCategory(forumDto));
	}

	public int deleteForumQuestion(ForumDto forumDto) {
		return forumMapper.deleteForumQuestion(forumDto);
	}
}
