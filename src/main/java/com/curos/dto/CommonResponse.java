package com.curos.dto;

public class CommonResponse {
	
	public static final Integer ERROR_CODE = -1;
	
	private Integer resultCode = 1;
	private String resultMsg = "Success";
	private Object contents;
	
	public Integer getResultCode() {
		return resultCode;
	}
	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public Object getContents() {
		return contents;
	}
	public void setContents(Object contents) {
		this.contents = contents;
	}
	public void setError(String errorMsg) {
		this.resultCode = ERROR_CODE;
		this.resultMsg = errorMsg;
	}
}
