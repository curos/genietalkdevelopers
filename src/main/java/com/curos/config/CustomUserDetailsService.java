package com.curos.config;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.curos.constant.Role;
import com.curos.dao.AccountMapper;
import com.curos.dto.AccountDto;

@Component
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private AccountMapper accountMapper;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		AccountDto account = accountMapper.selectAccountByEmail(email);
		if(account == null) {
			throw new UsernameNotFoundException("Not Exist User");
		}
		
		// ROLE CHECK
		List<Integer> roleList = accountMapper.selectAccountHasRole(account.getAccountId());
		if (!roleList.contains(Role.ROLE_DEVELOP.getRoleId())) {
			throw new UsernameNotFoundException("Not Developer User");
		}
		
		String password = account.getPassword();
		Date tempPasswdDt = account.getTempPasswdDate();
		if (tempPasswdDt != null && tempPasswdDt.after(new Date())) {
			password = account.getPassword() + " " + account.getTempPasswd();
		}
		
		return new User(account.getEmail(), password, AuthorityUtils.createAuthorityList("USER"));
	}
}
