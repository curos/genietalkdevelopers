package com.curos.constant;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.ModelAndView;

/**
 * 웹 페이지
 */
public enum WebPage {

	INDEX("/", "Hello World", null, "index")
	,LOGIN(UrlConstant.LOGIN, "로그인", null, "login")
	,MAIN(UrlConstant.MAIN, "Main Page", null, "main/main")
	,ACCOUNT_INFO(UrlConstant.ACCOUNT_INFO, "개인정보", null, "main/accountInfo")
	,CHANGE_PASSWORD(UrlConstant.CHNAGE_PASSWORD, "비밀번호 변경", null, "main/changePw")
	
	,APP_LIST(UrlConstant.APP_LIST, "내 애플리케이션", "목록", "app/list")
	,APP_ADD(UrlConstant.APP_ADD, "내 애플리케이션", "추가", "app/add")
	,APP_DETAIL(UrlConstant.APP_DETAIL, "내 애플리케이션", "상세", "app/detail")
	,APP_UPDATE(UrlConstant.APP_UPDATE, "내 애플리케이션", "수정", "app/update")
	,APP_ANALY(UrlConstant.APP_ANALY, "내 애플리케이션", "사용현황", "app/analy")
	
	// API 메뉴얼
	,API_DOC_VOICE_REC(UrlConstant.API_DOCUMENT + "/voiceRecognition", "자동통역 API메뉴얼", "음성인식", "apiDoc/api_doc_voice_rec")
	,API_DOC_VOICE_TRAN(UrlConstant.API_DOCUMENT + "/voiceTranslation", "자동통역 API메뉴얼", "음성통역", "apiDoc/api_doc_voice_tran")
	,API_DOC_TEXT_TRAN(UrlConstant.API_DOCUMENT + "/textTranslation", "자동통역 API메뉴얼", "문자번역", "apiDoc/api_doc_text_tran")
	,API_DOC_SUPPORT_LANG(UrlConstant.API_DOCUMENT + "/supportLanguages", "자동통역 API메뉴얼", "지원언어 목록", "apiDoc/api_doc_sup_lang")
	,API_DOC_RESPONSE_CODE(UrlConstant.API_DOCUMENT + "/responseCode", "자동통역 API메뉴얼", "응답코드", "apiDoc/api_doc_res_code")
	,API_DOC_LANG_LIST(UrlConstant.API_DOCUMENT + "/supportLangList", "자동통역 API메뉴얼", "지원하는 언어", "apiDoc/sup_lang_list")
	
	,API_LIST(UrlConstant.API_LIST, "자동통역 API메뉴얼", "지니톡 자동 통역 API 목록", "app/apiList")

	// 개발자 포럼
	,FORUM_LIST(UrlConstant.FORUM_LIST, "개발자포럼", "목록", "forum/list")
	,FORUM_ADD(UrlConstant.FORUM_ADD, "개발자포럼", "등록", "forum/add")
	,FORUM_DETAIL(UrlConstant.FORUM_DETAIL, "개발자포럼", "상세", "forum/detail")
	,FORUM_UPDATE(UrlConstant.FORUM_UPDATE, "개발자포럼", "수정", "forum/update")
	
	// 페이지 찾을 수 없음
	,NOT_FOUND_PAGE(null, null, null, null)
	;
	
	private String identity;
	private String title;
	private String subTitle;
	private String viewName;
	private String menu;
	
	private WebPage(String identity, String title, String subTitle, String viewName) {
		this.identity = identity;
		this.title = title;
		this.subTitle = subTitle;
		this.viewName = viewName;
		this.menu = this.name().toLowerCase();
	}

	public String getIdentity() {
		return identity;
	}

	public String getTitle() {
		return title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public String getViewName() {
		return viewName;
	}
	
	public String getMenu() {
		return menu;
	}

	public boolean isNotFoundPage() {
		return Optional.of(this).filter(wp -> {
			if (StringUtils.isEmpty(wp.getIdentity())
					|| StringUtils.isEmpty(wp.getViewName())) {
				return true;
			}
			return false;
		}).isPresent();
	}
	
	public ModelAndView getModelAndView() {
		ModelAndView mav = new ModelAndView(this.getViewName());
		mav.addObject("lgPanel", this.getTitle());
		mav.addObject("panel", this.getSubTitle());
		mav.addObject("menu", this.getMenu());
		return mav;
	}
	
	public static WebPage getWebPage(String identity) {
		WebPage[] webPages = WebPage.values();
		for (WebPage webPage : webPages) {
			if (StringUtils.equals(webPage.getIdentity(), identity)) {
				return webPage;
			}
			
			// 아래 웹페이지는 Prefix를 검사한다.
			if (webPage == APP_DETAIL || webPage == APP_UPDATE || webPage == APP_ANALY 
					|| webPage == FORUM_DETAIL || webPage == FORUM_UPDATE) {
				if (StringUtils.startsWith(identity, webPage.getIdentity())) {
					return webPage;
				}
			}
		}
		return NOT_FOUND_PAGE;
	}
	
}
