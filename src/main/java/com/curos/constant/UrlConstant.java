package com.curos.constant;

public class UrlConstant {
	public static final String MAIN = "/main";
	public static final String LOGIN = "/login";
	public static final String LOGOUT = "/logout";
	public static final String CHNAGE_PASSWORD = "/changePw";
	public static final String CONFIRM_PASSWORD = "/confirmPw";
	public static final String ACCOUNT_INFO = "/accountInfo";
	public static final String FORGOT_PASSWORD = "/forgotPw";
	public static final String API_DOCUMENT = "/apiDocument";
	public static final String API_LIST = "/apiList";
	
	public static final String ADD = "Reg";
    public static final String DELETE = "Del";
    public static final String LIST = "List";
    public static final String UPDATE = "Modi";
    public static final String DETAIL = "Detail";
    public static final String ANALY = "Analy";
    
	public static final String APP = "/myApp";
	public static final String APP_LIST = APP + LIST;
	public static final String APP_ADD = APP + ADD;
	public static final String APP_DELETE = APP + DELETE;
	public static final String APP_UPDATE = APP + UPDATE;
	public static final String APP_DETAIL = APP + DETAIL;
	public static final String APP_ANALY = APP + ANALY;
	
	public static final String FORUM = "/forum";
	public static final String FORUM_LIST = FORUM + LIST;
	public static final String FORUM_ADD = FORUM + ADD;
	public static final String FORUM_DETAIL = FORUM + DETAIL;
	public static final String FORUM_UPDATE = FORUM + UPDATE;
	public static final String FORUM_DELETE = FORUM + DELETE;
	
}
