package com.curos;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan(value="com.curos")
@EnableTransactionManagement
public class GenietalkdevelopersApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenietalkdevelopersApplication.class, args);
	}

}
