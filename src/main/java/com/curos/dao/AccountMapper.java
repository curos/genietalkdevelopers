package com.curos.dao;

import java.util.List;
import java.util.Map;

import com.curos.dto.AccountDto;

public interface AccountMapper {
	public int insertAccount(AccountDto account);
	public int insertAccountHasLanguage(Map<String, Object> langsData);
	public AccountDto selectAccountByEmail(String email);
	public int updateAccountTempPassword(Map<String, Object> paramMap);
	public int updateSecurityCode(Map<String, String> paramMap);
	public int updatePassword(Map<String, Object> paramMap);
	public List<Integer> selectAccountHasRole(int accountId);
}
