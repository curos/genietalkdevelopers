package com.curos.dao;

import java.util.List;
import java.util.Map;

import com.curos.dto.OauthClientDetailsDto;

public interface MyAppMapper {
	public int insertOauthClientDetail(OauthClientDetailsDto oauthClient);
	public List<OauthClientDetailsDto> selectOauthClientDetailByAccountId(int accountId);
	public OauthClientDetailsDto selectOauthClientDetailByClientId(Map<String, Object> paramMap);
	public int deleteOauthClientDetail(Map<String, Object> paramMap);
	public int updateOauthClientDetail(OauthClientDetailsDto oauthClientDetailsDto);
	public List<Map<String,Object>> selectAnalysis(Map<String, Object> paramMap);
}
