package com.curos.dao;

import java.util.List;
import java.util.Map;

import com.curos.dto.ForumDto;

public interface ForumMapper {
	public int selectForumListCount(Map<String, Object> searchMap);
	public List<ForumDto> selectForumList(Map<String, Object> searchMap);
	public List<Map<String, Object>> selectForumCategoryList();
	public int insertForumQuestion(ForumDto forumDto);
	public int insertForumCategory(ForumDto forumDto);
	public ForumDto selectForumDetail(int forumId);
	public int updateForumQuestion(ForumDto forumDto);
	public int updateForumCategory(ForumDto forumDto);
	public int deleteForumQuestion(ForumDto forumDto);
}
