package com.curos.dao;

import java.util.List;

import com.curos.dto.ApiDto;

public interface ApiMapper {
	public List<ApiDto> selectApiAllList();
}
