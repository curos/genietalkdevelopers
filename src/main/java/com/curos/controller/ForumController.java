package com.curos.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.AccountDto;
import com.curos.dto.CommonResponse;
import com.curos.dto.ForumDto;
import com.curos.service.AccountService;
import com.curos.service.ForumService;
import com.curos.util.PagingUtil;

@Controller
public class ForumController {
	@Autowired
	private AccountService accountService;

	@Autowired
	private ForumService forumService;
	
	@RequestMapping(value = UrlConstant.FORUM_LIST)
	public ModelAndView forumListView(@RequestParam(defaultValue="1") int currPage, 
			 @RequestParam(required = false) String categoryId,
			 @RequestParam(required = false) String field,
			 @RequestParam(required = false) String keyword,
			WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("categoryId", categoryId);
		searchMap.put("field", field);
		searchMap.put("keyword", keyword);
		
		List<Map<String, Object>> categoryList = forumService.selectForumCategoryList();
		
		int totalCount = forumService.getForumListCount(searchMap);
		PagingUtil page = new PagingUtil(currPage, totalCount);
		searchMap.put("page", page);

		List<ForumDto> forumList = forumService.getForumList(searchMap);

		mav.addObject("categoryList", categoryList);
		mav.addObject("forumList", forumList);
		mav.addObject("categoryId", categoryId);
		mav.addObject("field", field);
		mav.addObject("keyword", keyword);
		mav.addObject("page", page);
		return mav;
	}
	
	@RequestMapping(value = UrlConstant.FORUM_ADD, method = RequestMethod.GET)
	public ModelAndView forumAddView(WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		List<Map<String, Object>> categoryList = forumService.selectForumCategoryList();
		mav.addObject("categoryList", categoryList);
		return mav;
	}
	
	@RequestMapping(value = UrlConstant.FORUM_ADD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse forumAdd(@RequestParam int categoryId, @RequestParam(required = false) String title, 
			@RequestParam(required = false) String question, WebPage webPage, Principal principal) {
		CommonResponse response = new CommonResponse();
		
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		ForumDto forum = new ForumDto();
		forum.setAccountId(accountDto.getAccountId());
		forum.setTitle(title);
		forum.setQuestion(question);
		forum.setCategoryId(categoryId);
		
		int result = forumService.insertForumQuestion(forum);
		
		if(result < 1) {
			response.setError("등록 안됨");
		}
		
		return response;
	}
	
	@RequestMapping(value = UrlConstant.FORUM_DETAIL + "/{forumId}", method = RequestMethod.GET)
	public ModelAndView forumDetailView(@PathVariable int forumId, WebPage webPage, Principal principal) {
		ModelAndView mav = webPage.getModelAndView();
		
		ForumDto forum = forumService.getForumDetail(forumId);
		if(forum != null) {
			String question = Optional.ofNullable(forum.getQuestion()).map(str -> str.replaceAll("\r\n", "<br/>")).orElse("");
			String answer = Optional.ofNullable(forum.getAnswer()).map(str -> str.replaceAll("\r\n", "<br/>")).orElse("");
			forum.setQuestion(question);
			forum.setAnswer(answer);
		} else {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}
		mav = webPage.getModelAndView();
		mav.addObject("forum", forum);
		return mav;
	}

	@RequestMapping(value = UrlConstant.FORUM_UPDATE + "/{forumId}", method = RequestMethod.GET)
	public ModelAndView forumUpdateView(@PathVariable int forumId, WebPage webPage, Principal principal) {
		ModelAndView mav = webPage.getModelAndView();
		ForumDto forum = forumService.getForumDetail(forumId);
		if (forum != null) {
			String email = principal.getName();
			if (!email.equals(forum.getEmail())) {
				mav.addObject("error", "다른 사용자가 작성한 문의 내용은 수정하실수 없습니다.");
				return mav;
			}
			String answer = forum.getAnswer();
			if (answer != null && !"".equals(answer)) {
				mav.addObject("error", "이미 답변이 달린 글은 수정하실수 없습니다.");
				return mav;
			}
		} else {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}

		List<Map<String, Object>> categoryList = forumService.selectForumCategoryList();
		mav.addObject("categoryList", categoryList);
		mav.addObject("forum", forum);
		return mav;
	}

	@RequestMapping(value = UrlConstant.FORUM_UPDATE + "/{forumId}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse forumUpdate(@PathVariable int forumId, @RequestParam int categoryId, @RequestParam(required = false) String title, 
			@RequestParam(required = false) String question, WebPage webPage, Principal principal) {
		CommonResponse response = new CommonResponse();
		
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		ForumDto forum = new ForumDto();
		forum.setDevelperForumId(forumId);
		forum.setAccountId(accountDto.getAccountId());
		forum.setTitle(title);
		forum.setQuestion(question);
		forum.setCategoryId(categoryId);

		int result = forumService.updateForumQuestion(forum);

		if (result < 1) {
			response.setError("수정 안됨");
		}

		return response;
	}
	
	@RequestMapping(value = UrlConstant.FORUM_DELETE , method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse forumDelete(@RequestParam int forumId, Principal principal) {
		CommonResponse response = new CommonResponse();
		
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		ForumDto forum = new ForumDto();
		forum.setAccountId(accountDto.getAccountId());
		forum.setDevelperForumId(forumId);

		int result = forumService.deleteForumQuestion(forum);

		if (result < 1) {
			response.setError("삭제 안됨");
		}

		return response;
	}
}
