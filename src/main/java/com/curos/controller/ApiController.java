package com.curos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.ApiDto;
import com.curos.service.ApiService;

@Controller
public class ApiController {
	@Autowired
	private ApiService apiService;

	/**
	 * API 메뉴얼
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_DOCUMENT + "/{page}", method = RequestMethod.GET)
	public ModelAndView apiDocument(@PathVariable String page, WebPage webPage) {
		if (webPage.isNotFoundPage()) {
			return new ModelAndView(new RedirectView("/error/404"), null);
		}
		return webPage.getModelAndView(); 
	}
	
	/**
	 * API 목록
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_LIST, method = RequestMethod.GET)
	public ModelAndView apiList( WebPage webPage) {
		List<ApiDto> apiList = apiService.selectApiAllList();
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("apiList", apiList);
		return mav;
	}
}
