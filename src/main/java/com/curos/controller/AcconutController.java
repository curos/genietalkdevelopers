package com.curos.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Objects;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.curos.constant.AttributeConstant;
import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.AccountDto;
import com.curos.dto.CommonResponse;
import com.curos.service.AccountService;
import com.curos.service.EmailService;

import freemarker.template.TemplateException;
import io.reactivex.Observable;

@Controller
public class AcconutController {

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private AccountService accountService;

	@Autowired
	private EmailService emailService;

	@RequestMapping("/")
	public ModelAndView index(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * 로그인 뷰
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.LOGIN)
	public ModelAndView login(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * 메인 뷰
	 * @param model
	 * @param authentication
	 * @param session
	 * @param webPage
	 * @return
	 */
	@RequestMapping(UrlConstant.MAIN)
	public ModelAndView main(
			Authentication authentication
			, HttpSession session
			, WebPage webPage) {
		
		// TODO : 세션에 사용자 정보 설정 (적당한 위치가 아니다. 변경 필요)
		Object accountInfo = session.getAttribute(AttributeConstant.ACCOUNT_INFO);
		if (Objects.isNull(accountInfo)) {
			AccountDto accountDto = accountService.getAccountByEmail(authentication.getName());
			accountDto.setPassword(null);
			accountDto.setTempPasswd(null);
			accountDto.setTempPasswdDate(null);
			session.setAttribute(AttributeConstant.ACCOUNT_INFO, accountDto);			
		}
		
		/*
		 * 임시 비밀번호 로그인시 비밀번호 변경 화면으로 이동
		 */
		if (Optional.of(authentication)
				.map(Authentication::getDetails)
				.filter(obj -> obj instanceof String)
				.map(obj -> (String)obj)
				.filter(detail -> StringUtils.equals("TEMP_PW_LOGIN", detail))
				.map(detail -> Optional.ofNullable(session.getAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW)).isPresent())
				.filter(isRedirectChangePw -> isRedirectChangePw == false)
				.isPresent()) {
			session.setAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW, true);
			return new ModelAndView("redirect:" + UrlConstant.CHNAGE_PASSWORD);
		}
		
		return webPage.getModelAndView();
	}
	
	/**
	 * 비밀번호 분실
	 * @param email
	 * @param redirectAttributes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = UrlConstant.FORGOT_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse forgotPw(@RequestParam String email) throws Exception {
		CommonResponse response = new CommonResponse();
		
		if (!accountService.isExistAccount(email)) {
			response.setError("존재 하지 않는 계정 입니다.");
		} else {
			/*
			 * 임시 비밀번호 이메일 발송
			 */
			Observable<String> observable = emailService.sendTempPassword(email);
			observable.blockingSubscribe(
				(tempPassword) -> {
					accountService.updateTempPassword(email, tempPassword);
				}, (throwable) -> {
					throwable.printStackTrace();
					response.setError("임시 비밀번호 발급 실패");
				}, () -> {
					logger.info("임시 비번 발급 프로세스 종료");
				}
			);
		}
		
		
		return response;
	}
	
	/**
	 * 비밀번호 변경 뷰
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.CHNAGE_PASSWORD, method = RequestMethod.GET)
	public ModelAndView changePassword(WebPage webPage) {
		return webPage.getModelAndView();
	}
	
	/**
	 * 비밀번호 확인
	 * @param password
	 * @param model
	 * @param principal
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	@RequestMapping(value = UrlConstant.CONFIRM_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse confirmPassword(@RequestParam String password, Principal principal) throws MessagingException, IOException, TemplateException {
		CommonResponse response = new CommonResponse();
		String email = principal.getName();
		if (accountService.equalsPassword(email, password)) {
			/*
			 * 보안 코드 발송
			 */
			String securityCode = emailService.sendSecurityCode(email);
			
			/*
			 * 보안 코드 저장
			 */
			accountService.updateSecurityCode(email, securityCode);
		} else {
			response.setError("유효하지 않는 비밀번호 입니다.");
		}
		return response;
	}
	
	/**
	 * 신규 비밀번호 변경
	 * @param passwordOld
	 * @param passwordNew
	 * @param securityCode
	 * @param model
	 * @param principal
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = UrlConstant.CHNAGE_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse changePassword(
			@RequestParam String passwordOld
			, @RequestParam String passwordNew
			, @RequestParam String securityCode
			, Principal principal) {
		
		CommonResponse response = new CommonResponse();
		String email = principal.getName();
		if (!accountService.checkPassword(email, passwordOld, passwordNew)) {
			response.setError("유효하지 않는 비밀번호 입니다.");
		} else if (!accountService.checkSecurityCode(email, securityCode)) {
			response.setError("유효하지 않는 보안코드 입니다.");
		} else {
			accountService.updatePassword(email, passwordNew);
		}
		
		return response;
	}
	
	/**
	 * 계인정보 뷰
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.ACCOUNT_INFO, method = RequestMethod.GET)
	public ModelAndView accountInfo(Principal principal, WebPage webPage) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject(accountDto);
		return mav;
	}
	
	
}
