package com.curos.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.AccountDto;
import com.curos.dto.CommonResponse;
import com.curos.dto.OauthClientDetailsDto;
import com.curos.service.AccountService;
import com.curos.service.MyAppService;
import com.curos.util.DateTimeUtils;

import freemarker.template.TemplateException;

@Controller
public class MyAppController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private MyAppService myAppService;

	/**
	 * 앱 목록
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_LIST, method = RequestMethod.GET)
	public ModelAndView myAppList(WebPage webPage, Principal principal) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		List<OauthClientDetailsDto> myClientList = myAppService.getClientDetailByAccountId(accountDto.getAccountId());
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("myClientList", myClientList);
		return mav;
	}
	
	/**
	 * 앱 추가 뷰
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_ADD, method = RequestMethod.GET)
	public ModelAndView myAppAddView(WebPage webPage) {
		return webPage.getModelAndView();
	}
	
	/**
	 * 앱 추가 (client id, secret 발급)
	 * @param model
	 * @param appName
	 * @param appDescription
	 * @param principal
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	@RequestMapping(value = UrlConstant.APP_ADD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse myAppAdd(Model model, 
			@RequestParam String appName
			, @RequestParam String appDescription, Principal principal) throws MessagingException, IOException, TemplateException {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		OauthClientDetailsDto oauthClient = new OauthClientDetailsDto();
		oauthClient.setAppName(appName);
		oauthClient.setAppDescription(appDescription);
		
		int result = myAppService.registerOauthClient(accountDto, oauthClient);
		
		CommonResponse response = new CommonResponse();
		if(result < 1) {
			response.setError("삭제 안됨");
		}
		
		return response;
	}
	
	/**
	 * 앱 상세 뷰
	 * @param clientId
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_DETAIL + "/{clientId}", method = RequestMethod.GET)
	public ModelAndView myAppDetail(@PathVariable String clientId, WebPage webPage, Principal principal) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("accountId", accountDto.getAccountId());
		paramMap.put("clientId", clientId);

		OauthClientDetailsDto myClient = myAppService.getClientDetailByClientId(paramMap);
		String appDescription = myClient.getAppDescription();
		myClient.setAppDescription(appDescription.replaceAll("\r\n", "<br/>"));

		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("myClient", myClient);
		return mav;
	}
	
	/**
	 * 앱 업데이트 뷰
	 * @param clientId
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_UPDATE + "/{clientId}", method = RequestMethod.GET)
	public ModelAndView myAppUpdateView(@PathVariable String clientId, WebPage webPage, Principal principal) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("accountId", accountDto.getAccountId());
		paramMap.put("clientId", clientId);
		OauthClientDetailsDto myClient = myAppService.getClientDetailByClientId(paramMap);

		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("myClient", myClient);
		return mav;
	}
	
	/**
	 * 앱 정보 업데이트
	 * @param clientId
	 * @param appName
	 * @param appDescription
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_UPDATE + "/{clientId}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse myAppUpdate(@PathVariable String clientId, @RequestParam String appName
			, @RequestParam String appDescription, Model model, Principal principal) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		OauthClientDetailsDto oauthClient = new OauthClientDetailsDto();
		oauthClient.setAppName(appName);
		oauthClient.setAppDescription(appDescription);
		oauthClient.setClientId(clientId);
		oauthClient.setAccountId(accountDto.getAccountId());

		int result = myAppService.updateClientDetail(oauthClient);
		CommonResponse response = new CommonResponse();
		if(result < 1) {
			response.setError("수정 안됨");
		}
		
		return response;
	}
	
	/**
	 * 앱 삭제
	 * @param model
	 * @param clientId
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_DELETE, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse myAppDelete(Model model, @RequestParam String clientId, Principal principal) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		int result = myAppService.deleteClientDetail(accountDto, clientId);
		
		CommonResponse response = new CommonResponse();
		if(result < 1) {
			response.setError("삭제 안됨");
		}
		
		return response;
	}
	
	/**
	 * 앱 사용 현황 뷰
	 * @param searchDate
	 * @param clientId
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_ANALY + "/{clientId}", method = RequestMethod.GET)
	public ModelAndView myAppAnaly(
			@PathVariable String clientId
			, WebPage webPage
			, Principal principal) {
		String today = DateTimeUtils.getNowDateStr();
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("clientId", clientId);
		mav.addObject("start", today);
		mav.addObject("end", today);
		return mav;
	}
	
	/**
	 * 앱 사용 현황 데이터 조회
	 * @param searchDate
	 * @param clientId
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.APP_ANALY + "/{clientId}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse searchAppAnaly(
			@RequestParam(required=false) String searchDate
			, @PathVariable String clientId
			, Principal principal) {
		CommonResponse response = new CommonResponse();
		
		try {
			response.setContents(myAppService.selectAnalysis(principal.getName(), clientId, searchDate));
		} catch (Exception e) {
			response.setError(e.getMessage());
		}
		
		return response;
	}
}
