package com.curos.util;

public class PagingUtil {
	private static final int countPerPage = 10;  // 한 화면 당 게시글 수
	private static final int countPerBlock = 5;  // 한 화면 당 블럭 수
	
	private int currPage;  // 현재 페이지
	private int totalPage; // 총 페이지

	private int firstBlock; // 페이지에 봉여줄 첫 번호
	private int lastBlock;  // 페이지에 보여줄 끝 번호

	private int startRow; // 검색 시작번호
	private int endRow;   // 검색 끝번호

	public PagingUtil(int currPage, int totalCount) {
		this.currPage = currPage;
		this.totalPage = ((totalCount % countPerPage) == 0) ? totalCount / countPerPage : totalCount / countPerPage + 1;
		this.startRow = (currPage - 1) * countPerPage + 1;
		this.endRow = startRow + countPerPage - 1;
		
		this.firstBlock = ((int)(currPage - 1) / countPerBlock) * countPerBlock + 1;
		this.lastBlock = firstBlock + countPerBlock - 1;
		if(lastBlock > totalPage) {
			lastBlock = totalPage;
		}
	}

	public int getCurrPage() {
		return currPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public int getFirstBlock() {
		return firstBlock;
	}

	public int getLastBlock() {
		return lastBlock;
	}

	public int getStartRow() {
		return startRow;
	}

	public int getEndRow() {
		return endRow;
	}
}
