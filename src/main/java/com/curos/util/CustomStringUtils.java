package com.curos.util;

import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomStringUtils {

	/**
	 * 비밀번호 정규식
	 * 영문(대소문자)1개, 숫자1개, 특수문자1개, 길이 6~16자
	 */
	public static final String PASSWORD_PATTERN = "^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,16}$";
	
	private CustomStringUtils() {
		throw new AssertionError("DO NOT CREATE INSTANCE");
	}

	/**
	 * 랜덤 문자열 생성
	 * @param digit
	 * @param mode
	 * @return
	 */
	public static String random(int digit, RandomMode mode) {
		StringBuffer temp = new StringBuffer();
		Random rnd = new Random();
		for (int i = 0; i < digit; i++) {
			int rIndex = 0;
			if (mode == RandomMode.ALL) {
				rIndex = rnd.nextInt(3);
			} else {
				rIndex = mode.ordinal();
			}
			switch (rIndex) {
			case 0:
				// a-z
				temp.append((char) ((int) (rnd.nextInt(26)) + 97));
				break;
			case 1:
				// A-Z
				temp.append((char) ((int) (rnd.nextInt(26)) + 65));
				break;
			case 2:
				// 0-9
				temp.append((rnd.nextInt(10)));
				break;
			}
		}
		return temp.toString();
	}
	
	/**
	 * 비밀번호 규칙 검사
	 * @param password
	 * @return 유효하면 true, 유효하지않으면 false
	 */
	public static boolean validatePasswordRule(String password) {
		
		Optional<String> pwOptional = Optional.ofNullable(password);
		return pwOptional.map(pw -> {
			Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
			Matcher matcher = pattern.matcher(pw);
			return matcher.matches();
		}).orElse(false);
	}
	
	/**
	 * 랜덤 생성 모드
	 * @see CustomStringUtils.random
	 */
	public static enum RandomMode {
		ALPHABET_SMALL
		, ALPHABET_CAPTIAL
		, NUMBER
		, ALL
	}
}
