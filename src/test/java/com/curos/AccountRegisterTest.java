package com.curos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.curos.dto.AccountDto;
import com.curos.dto.OauthClientDetailsDto;
import com.curos.service.AccountService;
import com.curos.service.MyAppService;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "local")
@SpringBootTest
public class AccountRegisterTest {

	@Autowired
	MyAppService myAppService;

	@Autowired
	AccountService accountService;
	
	@Test
	public void registerOauthClient() {
		// Client ID와 Secret을 발급해서 Oauth에 저장
		// TODO : 이 테스트를 하려면 settingOauthClientData 함수에서 값을 설정해주세요
		OauthClientDetailsDto oauthClient = settingOauthClientData();
		System.out.println("clientId : " + oauthClient.getClientId());
		System.out.println("clientSecret : " + oauthClient.getClientSecret());

		int result = myAppService.registerOauthClient(oauthClient);
		assertEquals(1, result);
	}

	@Test
	public void registerAccount() {
		// Account에 회원 저장
		AccountDto accountDto = settingAccountData();
		int result = accountService.registerAccount(accountDto);
		assertNotNull(accountDto.getAccountId());
		assertEquals(1, result);
	}
	
	@Test
	public void insertAccountHasLanguageTest() {
		// Account의 언어 목록 저장
		// TODO : 이 테스트를 하려면 accountId 값을 입력해주세요
		int accountId = 1;
		List<Integer> languages = new ArrayList<Integer>();
		languages.add(1);
		languages.add(2);
		languages.add(3);
		
		int result = accountService.insertAccountHasLanguage(accountId, languages);
		assertEquals(languages.size(), result);
	}

	@Test
	public void getPassword() {
		String company = "Curos";
		String clientId = myAppService.getClientId(company);
		String clientSecret = myAppService.getClientSecret();
		System.out.println(clientId);
		System.out.println(clientSecret);
	}

	public AccountDto settingAccountData() {
		AccountDto account = new AccountDto();
		account.setCompany("Curos");
		account.setEmail("hjahn@curos.co.kr");
		account.setMobile("010-9123-0486");
		account.setName("안희진");
		account.setPassword("asdf");
		account.setTelephone("010-9123-0486");
		return account;
	}

	public OauthClientDetailsDto settingOauthClientData() {
		OauthClientDetailsDto oauthClient = new OauthClientDetailsDto();

		// #################################
		// TODO : 아래 사항을 입력해주세요.
		String company = "curos";
		int accountId = 1;
		// #################################

		oauthClient.setClientId(myAppService.getClientId(company));
		oauthClient.setClientSecret(myAppService.getClientSecret());
		oauthClient.setScope("read, write");
		oauthClient.setAuthorizedGrantTypes("client_credentials,refresh_token");
		oauthClient.setAccessTokenValidity(60 * 60 * 4);
		oauthClient.setAccountId(accountId);

		return oauthClient;
	}
	
	@Test
	public void validatePassword() {
		boolean isPass = accountService.checkPassword("hsmini3@curos.co.kr", "curos788!", "curos788!@");
		assertTrue(isPass);
	}
}
