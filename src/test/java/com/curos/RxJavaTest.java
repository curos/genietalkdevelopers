package com.curos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.curos.constant.WebPage;
import com.curos.dto.CommonResponse;
import com.curos.service.AccountService;
import com.curos.service.EmailService;
import com.curos.service.MyAppService;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "local")
@SpringBootTest
public class RxJavaTest {
	
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private AccountService accountService;
	
	@Test
	public void sendTempPassword() {
		
		String email = "hsmini3@curos.co.kr";
		CommonResponse response = new CommonResponse();
		
		Observable<String> observable = emailService.sendTempPassword(email);
		observable.blockingSubscribe(
			(tempPassword) -> {
				logger.info("임시 비번 저장");
				accountService.updateTempPassword(email, tempPassword);
			}, (throwable) -> {
				logger.error("임시 비번 발급 실패");
				response.setError("임시 비밀번호 발급 실패");
			}, () -> {
				logger.info("프로세스 종료");
			}
		);
		
		logger.info("임시 비번 전송 및 저장 완료? " + response.getResultMsg());
	}
	
	@Test
	public void rxJavaSingleTest() throws Exception {
		
		boolean isBlocking = true;
		
		Consumer<Object> onSuccess = (str) -> {
			logger.info("Success");
		};
		
		Consumer<Throwable> onError = (str) -> {
			logger.info("Error");
		};
		
		Single<String> single = Single.fromCallable(() -> {
			logger.info("메인 동작");
			return "asdf";
		})
		.subscribeOn(Schedulers.io())
		.map((result) -> {
			logger.info("map");
			return "result : " + result;
		});
		
		if (isBlocking) {
			String result = single.blockingGet();
			try {
				onSuccess.accept(result);
			} catch (Exception e) {
				onError.accept(e);
			}
		} else {
			single.subscribe(onSuccess, onError);
		}
		
		logger.info("동작끝~");
	}
	
	@Test
	public void checkSecurityCode() {
		boolean isExist = accountService.checkSecurityCode("hsmini3@curos.co.kr", "103077");
		assertTrue(isExist);
	}
	
	@Test
	public void checkPassword() {
		boolean isGood = accountService.equalsPassword("hsmini3@curos.co.kr", "mX4hv74WyFk67SR");
		assertTrue("비밀번호가 일치하지 않습니다.", isGood);
	}
	
	@Test
	public void getWebPage() {
		WebPage webPage = WebPage.getWebPage("voiceRecognition");
		assertEquals(WebPage.API_DOC_VOICE_REC, webPage);
		
		webPage = WebPage.getWebPage("asdfasdf");
		assertEquals(WebPage.NOT_FOUND_PAGE, webPage);
	}
	
	@Test
	public void flow() throws InterruptedException {
        final String tmpStr = Arrays.stream(new String[10_000_000]).map(x->"*").collect(Collectors.joining());
        Flowable<String> foo = Flowable.range(0, 1000_000_000)
                .map(x-> {
                    System.out.println("[very fast sender] i'm fast. very fast.");
                    System.out.println(String.format("sending id: %s %d%50.50s", Thread.currentThread().getName(), x, tmpStr));
                    return x+tmpStr;
                });
 
        foo.observeOn(Schedulers.computation()).subscribe(x->{
            Thread.sleep(1000);
            System.out.println("[very busy receiver] i'm busy. very busy.");
            System.out.println(String.format("receiving id: %s %50.50s", Thread.currentThread().getName(), x));
        });
 
        while (true) {
            Thread.sleep(1000);
        }
	}
	
	@Test
	public void flow2() throws InterruptedException {
        final String tmpStr = Arrays.stream(new String[10_000_000]).map(x->"*").collect(Collectors.joining());
        Observable<String> foo = Observable.range(0, 1000_000_000)
                .map(x-> {
                    System.out.println("[very fast sender] i'm fast. very fast.");
                    System.out.println(String.format("sending id: %s %d%50.50s", Thread.currentThread().getName(), x, tmpStr));
                    return x+tmpStr;
                });
 
        foo.observeOn(Schedulers.computation()).subscribe(x->{
            Thread.sleep(1000);
            System.out.println("[very busy receiver] i'm busy. very busy.");
            System.out.println(String.format("receiving id: %s %50.50s", Thread.currentThread().getName(), x));
        });
 
        while (true) {
            Thread.sleep(1000);
        }
	}
	
	@Autowired
	private MyAppService myAppService;
	
	@Test
	public void appAnalysis() throws Exception {
		List<Map<String, Object>> resultList = myAppService.selectAnalysis("hsmini3@curos.co.kr"
				, "7YGQ66Gc7IqkMjAxODA2MTgwNDMxMTE="
				, "2018/06/01 - 2018/06/19");
		assertNotNull(resultList);
		assertFalse(resultList.isEmpty());
		resultList.forEach(map -> {
			map.forEach((key, value)->{
				logger.info(key + " : " + value);
			});
		});
	}
	
}
