package com.curos;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;

import com.curos.util.DateTimeUtils;

public class OptionalTest {

	@Test
	public void optionalMapVsFlatMap() {
		/*
		 * Optional<String> == Optional<String>
		 */
		assertEquals(
				Optional.of("test"), 
				Optional.of("test")
						.map(s -> s.toUpperCase())
						.map(s -> s.toLowerCase())
		);
	}
	
	@Test
	public void optionalMapVsFlatMap2() {
		/*
		 * Optional<Optional<String>> == Optional<Optional<String>>
		 */
		assertEquals(
				Optional.of(Optional.of("STRING")), 
				Optional.of("STRING")
						.map(s -> Optional.of(s.toLowerCase()))
						.map(o -> o.map(s -> s.toUpperCase()))
		);
	}
	
	@Test
	public void optionalMapVsFlatMap3() {
		/*
		 * Optional<String> == Optional<String>
		 */
		assertEquals(Optional.of("STRING"), 
				Optional.of("STRING")
				.flatMap(s -> Optional.of(s.toLowerCase()))
				.map(s -> s.toUpperCase())
		);
	}
	
	@Test
	public void javaTime() {
		
		String nowDateTime = DateTimeUtils.getNowDateTimeStr();
		System.out.println("현재 날짜시간 : " + nowDateTime);
		
		String nowDate = DateTimeUtils.getNowDateStr();
		System.out.println("현재 날짜 : " + nowDate);
	}

}
